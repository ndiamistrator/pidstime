#!/bin/sh
# sh / bash

pidstime() {
    local _pid=$1 _pst=$2 _PST= _code
    _pidstime "$@" && _code=0 || _code=$?
    __=$_PST; return $_code
}

_pidstime() {
    local _S= _L
    if { while read -r _L; do _S="$_S $_L"
    done <"/proc/$_pid/stat"; } 2>/dev/null; then
        set -- ${_S##*)}; _PST=${20}
        [ "$_PST" ] ||                   return 4
        if [ "$_PST" = "$_pst" ]; then   return 0
        elif [ "$_pst" ]; then           return 1
        else printf "%s\n" "$_PST" || true    # >
        fi;                              return 0
    elif [ -e "/proc/$_pid/stat" ]; then return 2
    fi;                                  return 1
}

# pidstime "$@"
