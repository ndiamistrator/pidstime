#!/bin/sh
# sh / bash
pidstime() {
    local _pid=$1 _pst=$2 _PST= _code
    _pidstime "$@" && _code=0 || _code=$?
    __=$_PST; return $_code
}
_pidstime() {
    local _S= _L
    if { while read -r _L; do _S="$_S $_L"
    done <"/proc/$_pid/stat"; } 2>/dev/null; then
        set -- ${_S##*)}; _PST=${20}
        [ "$_PST" ] ||                   return 4
        if [ "$_PST" = "$_pst" ]; then   return 0
        elif [ "$_pst" ]; then           return 1
        else printf "%s\n" "$_PST" || true    # >
        fi;                              return 0
    elif [ -e "/proc/$_pid/stat" ]; then return 2
    fi;                                  return 1
}
#pidstime "$@"

#!/bin/bash
# bash only
#pidstime() {
#    local pid=$1
#    local stat=$(cat "/proc/$pid/stat" 2>/dev/null || true)
#    if [ $# -gt 1 ]; then
#        local time=$2
#        if [ -z "$stat" ]; then
#            return -1
#        elif [[ "$stat" =~ ^.*\)(\ [^\ ]+){19}\ ([0-9]+) ]] \
#             && [ "${BASH_REMATCH[2]}" = "$time" ]; then
#            return 0
#        else
#            return 1
#        fi
#    else
#        # PIDSTIME='awk '\''{print gensub("^.*\\)( [^ ]+){19} ([0-9]+).*", "\\2", 1)}'\'
#        if [ -z "$stat" ]; then
#            echo -1
#        elif [[ "$stat" =~ ^.*\)(\ [^\ ]+){19}\ ([0-9]+) ]]; then
#            echo ${BASH_REMATCH[2]}
#        else
#            echo 0
#        fi
#    fi
#}
