from math import log
from random import random

BASE = 10

Max_log_SC_CLK_TCK = 9
Max_log_Btime = Max_log_SC_CLK_TCK + 3
Max_log_Btime_Resolution = -Max_log_SC_CLK_TCK - 3

Jitters = 1000

stats = {}

if __name__ == '__main__':
    for _neg_log_btime_resolution in range(-Max_log_Btime_Resolution):
        _log_btime_resolution = -_neg_log_btime_resolution
        for log_sc_clk_tck in range(Max_log_SC_CLK_TCK):
            SC_CLK_TCK = BASE**log_sc_clk_tck
            err = []
            for log_btime in range(Max_log_Btime):
                for _ in range(Jitters):
                    jitter = random()
                    btime = BASE**(log_btime + jitter)
                    _btime = round(btime, -_log_btime_resolution)
                    if not _btime:
                        continue
                    stime = round(btime*SC_CLK_TCK)
                    if not stime:
                        continue
                    sc_clk_tck = round(stime/_btime)

                    if sc_clk_tck != SC_CLK_TCK:
                        err.append(btime)

            expect = SC_CLK_TCK * BASE**(_log_btime_resolution)
            sample = max(err) if err else 1 if log_sc_clk_tck == -_log_btime_resolution else 0
            stats[(_log_btime_resolution, SC_CLK_TCK)] = expect, sample



    rows = [['resolution', 'SC_CLK_TCK', 'expect', 'sample']]
    for (_log_btime_resolution, SC_CLK_TCK), (expect, sample) in stats.items():
        rows.append([f'{BASE}**{_log_btime_resolution}', SC_CLK_TCK, f'{expect:.3f}', f'{sample:.3f}'])

    widths = [max(map(len, map(str, (row[col] for row in rows))))
              for col in range(max(map(len, rows)))]

    print('''\
Expected & sampled MINIMUM boot time needed
for guaranteed correct calculation of SC_CLK_TCK
per reported boot time min. resolution & calculated SC_CLK_TCK

/proc/uptime     -> ${1} (uptime)
/proc/stat       -> [btime]
/proc/<PID>/stat -> ${22} (stime)

Formula: BTIME / MIN_BTIME_RESOLUTION < SC_CLK_TCK
Where:   SC_CLK_TCK = STIME / UPTIME

Assuming that STIME is sampled near enough (how much near?) correspoding UPTIME:
Maybe:
    Take sample STIME_1
    Take sample UPTIME_1
    Take sample UPTIME_2
    Take sample STIME_2
    ...
    ''')  # TODO

    for row in rows:
        print(' | '.join(f'{cell:>{width}}'
                         for cell, width in zip(row, widths)))

# Expected & sampled MINIMUM boot time needed
# for guaranteed correct calculation of SC_CLK_TCK
# per reported boot time min. resolution & calculated SC_CLK_TCK
# 
# /proc/uptime     -> ${1} (uptime)
# /proc/stat       -> [btime]
# /proc/<PID>/stat -> ${22} (stime)
# 
# Formula: BTIME / MIN_BTIME_RESOLUTION < SC_CLK_TCK
# Where:   SC_CLK_TCK = STIME / UPTIME
# 
# Assuming that STIME is sampled near enough (how much near?) correspoding UPTIME:
# Maybe:
#     Take sample STIME_1
#     Take sample UPTIME_1
#     Take sample UPTIME_2
#     Take sample STIME_2
# TODO.......................................................................
# 
# resolution | SC_CLK_TCK |        expect |       sample
#      10**0 |          1 |         1.000 |        1.000
#      10**0 |         10 |        10.000 |        9.494
#      10**0 |        100 |       100.000 |       97.495
#      10**0 |       1000 |      1000.000 |      984.507
#      10**0 |      10000 |     10000.000 |     9881.504
#      10**0 |     100000 |    100000.000 |    91007.467
#      10**0 |    1000000 |   1000000.000 |   961780.484
#      10**0 |   10000000 |  10000000.000 |  9448839.504
#      10**0 |  100000000 | 100000000.000 | 93457270.483
#     10**-1 |          1 |         0.100 |        0.000
#     10**-1 |         10 |         1.000 |        1.000
#     10**-1 |        100 |        10.000 |        9.948
#     10**-1 |       1000 |       100.000 |       96.949
#     10**-1 |      10000 |      1000.000 |      903.649
#     10**-1 |     100000 |     10000.000 |     9608.750
#     10**-1 |    1000000 |    100000.000 |    92106.548
#     10**-1 |   10000000 |   1000000.000 |   991347.750
#     10**-1 |  100000000 |  10000000.000 |  9080029.949
#     10**-2 |          1 |         0.010 |        0.000
#     10**-2 |         10 |         0.100 |        0.000
#     10**-2 |        100 |         1.000 |        1.000
#     10**-2 |       1000 |        10.000 |        9.915
#     10**-2 |      10000 |       100.000 |       84.204
#     10**-2 |     100000 |      1000.000 |      932.065
#     10**-2 |    1000000 |     10000.000 |     9118.015
#     10**-2 |   10000000 |    100000.000 |    92819.875
#     10**-2 |  100000000 |   1000000.000 |   993365.145
#     10**-3 |          1 |         0.001 |        0.000
#     10**-3 |         10 |         0.010 |        0.000
#     10**-3 |        100 |         0.100 |        0.000
#     10**-3 |       1000 |         1.000 |        1.000
#     10**-3 |      10000 |        10.000 |        9.847
#     10**-3 |     100000 |       100.000 |       95.893
#     10**-3 |    1000000 |      1000.000 |      940.460
#     10**-3 |   10000000 |     10000.000 |     9554.959
#     10**-3 |  100000000 |    100000.000 |    98700.194
#     10**-4 |          1 |         0.000 |        0.000
#     10**-4 |         10 |         0.001 |        0.000
#     10**-4 |        100 |         0.010 |        0.000
#     10**-4 |       1000 |         0.100 |        0.000
#     10**-4 |      10000 |         1.000 |        1.000
#     10**-4 |     100000 |        10.000 |        9.876
#     10**-4 |    1000000 |       100.000 |       92.558
#     10**-4 |   10000000 |      1000.000 |      946.082
#     10**-4 |  100000000 |     10000.000 |     9138.147
#     10**-5 |          1 |         0.000 |        0.000
#     10**-5 |         10 |         0.000 |        0.000
#     10**-5 |        100 |         0.001 |        0.000
#     10**-5 |       1000 |         0.010 |        0.000
#     10**-5 |      10000 |         0.100 |        0.000
#     10**-5 |     100000 |         1.000 |        1.000
#     10**-5 |    1000000 |        10.000 |        9.887
#     10**-5 |   10000000 |       100.000 |       93.444
#     10**-5 |  100000000 |      1000.000 |      887.894
#     10**-6 |          1 |         0.000 |        0.000
#     10**-6 |         10 |         0.000 |        0.000
#     10**-6 |        100 |         0.000 |        0.000
#     10**-6 |       1000 |         0.001 |        0.000
#     10**-6 |      10000 |         0.010 |        0.000
#     10**-6 |     100000 |         0.100 |        0.000
#     10**-6 |    1000000 |         1.000 |        1.000
#     10**-6 |   10000000 |        10.000 |        9.999
#     10**-6 |  100000000 |       100.000 |       96.575
#     10**-7 |          1 |         0.000 |        0.000
#     10**-7 |         10 |         0.000 |        0.000
#     10**-7 |        100 |         0.000 |        0.000
#     10**-7 |       1000 |         0.000 |        0.000
#     10**-7 |      10000 |         0.001 |        0.000
#     10**-7 |     100000 |         0.010 |        0.000
#     10**-7 |    1000000 |         0.100 |        0.000
#     10**-7 |   10000000 |         1.000 |        1.000
#     10**-7 |  100000000 |        10.000 |        9.709
#     10**-8 |          1 |         0.000 |        0.000
#     10**-8 |         10 |         0.000 |        0.000
#     10**-8 |        100 |         0.000 |        0.000
#     10**-8 |       1000 |         0.000 |        0.000
#     10**-8 |      10000 |         0.000 |        0.000
#     10**-8 |     100000 |         0.001 |        0.000
#     10**-8 |    1000000 |         0.010 |        0.000
#     10**-8 |   10000000 |         0.100 |        0.000
#     10**-8 |  100000000 |         1.000 |        1.000
#     10**-9 |          1 |         0.000 |        0.000
#     10**-9 |         10 |         0.000 |        0.000
#     10**-9 |        100 |         0.000 |        0.000
#     10**-9 |       1000 |         0.000 |        0.000
#     10**-9 |      10000 |         0.000 |        0.000
#     10**-9 |     100000 |         0.000 |        0.000
#     10**-9 |    1000000 |         0.001 |        0.000
#     10**-9 |   10000000 |         0.010 |        0.000
#     10**-9 |  100000000 |         0.100 |        0.000
#    10**-10 |          1 |         0.000 |        0.000
#    10**-10 |         10 |         0.000 |        0.000
#    10**-10 |        100 |         0.000 |        0.000
#    10**-10 |       1000 |         0.000 |        0.000
#    10**-10 |      10000 |         0.000 |        0.000
#    10**-10 |     100000 |         0.000 |        0.000
#    10**-10 |    1000000 |         0.000 |        0.000
#    10**-10 |   10000000 |         0.001 |        0.000
#    10**-10 |  100000000 |         0.010 |        0.000
#    10**-11 |          1 |         0.000 |        0.000
#    10**-11 |         10 |         0.000 |        0.000
#    10**-11 |        100 |         0.000 |        0.000
#    10**-11 |       1000 |         0.000 |        0.000
#    10**-11 |      10000 |         0.000 |        0.000
#    10**-11 |     100000 |         0.000 |        0.000
#    10**-11 |    1000000 |         0.000 |        0.000
#    10**-11 |   10000000 |         0.000 |        0.000
#    10**-11 |  100000000 |         0.001 |        0.000
