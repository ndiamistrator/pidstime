#!/bin/env python3

'''
    Creation time of process by pid, in millisecods since the epoch.
'''

__all__ = 'pidstime', 'PidsTimeError', 'Unreadable', 'Incompatible'


import os
import sys


class PidsTimeError(Exception):      exit_code = 16
class Unreadable(PidsTimeError):     exit_code = 2
class Incompatible(PidsTimeError):   exit_code = 4
class NonExistent(PidsTimeError):    exit_code = 1


if os.name == 'nt':
    from ctypes import (Structure as _ctypes_Structure,
                        byref as _ctypes_byref,
                        windll as _ctypes_windll,
                        c_ulonglong as _ctypes_c_ulonglong)
    from ctypes.wintypes import WORD as _ctypes_wintypes_WORD
    from datetime import datetime as _datetime_datetime, timezone as _datetime_timezone

    _PROCESS_QUERY_INFORMATION = 0x0400
    _FALSE = 0

    class _WinError(PidsTimeError): pass

    class _SYSTEMTIME(_ctypes_Structure):
        _fields_ = [
            ('wYear', _ctypes_wintypes_WORD),
            ('wMonth', _ctypes_wintypes_WORD),
            ('wDayOfWeek', _ctypes_wintypes_WORD),
            ('wDay', _ctypes_wintypes_WORD),
            ('wHour', _ctypes_wintypes_WORD),
            ('wMinute', _ctypes_wintypes_WORD),
            ('wSecond', _ctypes_wintypes_WORD),
            ('wMilliseconds', _ctypes_wintypes_WORD),
        ]

    def pidstime(pid):
        try:
            handle = _ctypes_windll.kernel32.OpenProcess(_PROCESS_QUERY_INFORMATION, _FALSE, pid)
            if not handle:
                raise NonExistent(pid)
            try:
                creationtime = _ctypes_c_ulonglong()
                other = _ctypes_c_ulonglong()
                if not _ctypes_windll.kernel32.GetProcessTimes(
                        handle,
                        _ctypes_byref(creationtime),
                        _ctypes_byref(other),
                        _ctypes_byref(other),
                        _ctypes_byref(other)):
                    raise _WinError('GetProcessTimes()')
                systime = _SYSTEMTIME()
                if not _ctypes_windll.kernel32.FileTimeToSystemTime(
                        _ctypes_byref(creationtime),
                        _ctypes_byref(systime)):
                    raise _WinError('FileTimeToSystemTime()')
            finally:
                try:
                    _ctypes_windll.kernel32.CloseHandle(handle)
                except Exception:
                    pass
        except PidsTimeError:
            raise
        except Exception:
            raise Unreadable(pid)

        try:
            dt = _datetime_datetime(systime.wYear,
                           systime.wMonth,
                           systime.wDay,
                           systime.wHour,
                           systime.wMinute,
                           systime.wSecond,
                           systime.wMilliseconds * 1000,
                           _datetime_timezone.utc)
            return int(dt.timestamp()*1000)
        except Exception:
            raise Incompatible(pid, creationtime)

    # from win32api import OpenProcess as _OpenProcess
    # from win32con import PROCESS_QUERY_INFORMATION as _PROCESS_QUERY_INFORMATION
    # from win32process import GetProcessTimes as _GetProcessTimes
    # from pywintypes import FALSE as _FALSE
    # 
    # def pidstime(pid):
    #     try:
    #         handle = _OpenProcess(_PROCESS_QUERY_INFORMATION, _FALSE, pid)
    #         times = _GetProcessTimes(handle)
    #     except Exception:
    #         raise Unreadable(pid)
    #     try:
    #         pidstime = times['CreationTime']
    #         return pidstime.timestamp()
    #     except Exception:
    #         raise Incompatible(pid, times)


elif os.name == 'posix':
    try:
        def _init():
            global _BTIME
            with open('/proc/stat', 'rb') as _stat:
                while True:
                    line = _stat.readline()
                    if not line: break
                    key, _, val = line.partition(b' ')
                    if key == b'btime':
                        _BTIME = float(val)
            with open('/proc/%s/stat'%(os.getpid(),), 'rb'):
                pass
        _init()
        del _init

        _SC_CLK_TCK = os.sysconf('SC_CLK_TCK')

        def pidstime(pid):
            path = '/proc/%s/stat'%(pid,)
            try:
                with open(path, 'rb') as _stat:
                    stat = _stat.read()
            except FileNotFoundError:
                raise NonExistent(pid)
            except Exception:
                raise Unreadable(pid)

            try:
                after_cmd = stat.rpartition(b')')[2]
                spidstime = after_cmd.split(b' ')[20]
                return int((float(spidstime)/_SC_CLK_TCK + _BTIME) * 1000 + .5)
            except Exception:
                raise Incompatible(pid, stat)

    except Exception:
        try:
            import psutil
        except Exception:
            raise ImportError('cannot get system boot time')

        def pidstime(pid):
            try:
                return int(psutil.Process(int(pid)).create_time() * 1000 + .5)
            except psutil.NoSuchProcess:
                raise NonExistent(pid)
            except Exception:
                raise Unreadable(pid)

else:
    raise NotImplementedError('incompatible system: %s'%(os.name,))


def _parser(parser=None, prog=None):
    if parser is None:
        import argparse
        parser = argparse.ArgumentParser(prog=prog)
    else:
        assert prog is None
    parser.add_argument('pid', type=int)
    parser.add_argument('pidstime', nargs='?', type=int)
    return parser


class _ArgumentsError(PidsTimeError): exit_code = 64
class _OutputError(PidsTimeError):    exit_code = 32


def _main_(argv=None, prog=None):
    parser = _parser(prog=prog)
    try:
        args = parser.parse_args(argv)
    except:
        raise _ArgumentsError(argv)

    try:
        _pidstime = pidstime(args.pid)
    except NonExistent as E:
        return E.exit_code

    if args.pidstime is None:
        try:
            print(_pidstime)
        except Exception:
            raise _OutputError(_pidstime)

    elif _pidstime == args.pidstime:
        return 0

    else:
        return 1

    # return None


def _main():
    try:
        exit_code = _main_()
    except BaseException as E:
        import traceback
        try:
            traceback.print_exc()
        except:
            sys.exit(PidsTimeError.exit_code)
        if isinstance(E, PidsTimeError):
            sys.exit(E.exit_code)
        else:
            sys.exit(PidsTimeError.exit_code)
    else:
        sys.exit(exit_code)


if __name__ == '__main__':
    _main()
